#include <gio/gio.h>

#if defined (__ELF__) && ( __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 6))
# define SECTION __attribute__ ((section (".gresource.balsa_ab"), aligned (8)))
#else
# define SECTION
#endif

#ifdef _MSC_VER
static const SECTION union { const guint8 data[717]; const double alignment; void * const ptr;}  balsa_ab_resource_data = { {
  0107, 0126, 0141, 0162, 0151, 0141, 0156, 0164, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 
  0030, 0000, 0000, 0000, 0254, 0000, 0000, 0000, 0000, 0000, 0000, 0050, 0005, 0000, 0000, 0000, 
  0000, 0000, 0000, 0000, 0002, 0000, 0000, 0000, 0002, 0000, 0000, 0000, 0005, 0000, 0000, 0000, 
  0005, 0000, 0000, 0000, 0113, 0120, 0220, 0013, 0001, 0000, 0000, 0000, 0254, 0000, 0000, 0000, 
  0004, 0000, 0114, 0000, 0260, 0000, 0000, 0000, 0264, 0000, 0000, 0000, 0324, 0265, 0002, 0000, 
  0377, 0377, 0377, 0377, 0264, 0000, 0000, 0000, 0001, 0000, 0114, 0000, 0270, 0000, 0000, 0000, 
  0274, 0000, 0000, 0000, 0012, 0067, 0312, 0360, 0004, 0000, 0000, 0000, 0274, 0000, 0000, 0000, 
  0012, 0000, 0166, 0000, 0310, 0000, 0000, 0000, 0262, 0002, 0000, 0000, 0124, 0214, 0331, 0133, 
  0000, 0000, 0000, 0000, 0262, 0002, 0000, 0000, 0010, 0000, 0114, 0000, 0274, 0002, 0000, 0000, 
  0300, 0002, 0000, 0000, 0151, 0377, 0210, 0203, 0003, 0000, 0000, 0000, 0300, 0002, 0000, 0000, 
  0010, 0000, 0114, 0000, 0310, 0002, 0000, 0000, 0314, 0002, 0000, 0000, 0157, 0162, 0147, 0057, 
  0003, 0000, 0000, 0000, 0057, 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0141, 0142, 0055, 0155, 
  0141, 0151, 0156, 0056, 0165, 0151, 0000, 0000, 0146, 0016, 0000, 0000, 0001, 0000, 0000, 0000, 
  0170, 0332, 0325, 0227, 0115, 0117, 0303, 0060, 0014, 0206, 0357, 0374, 0212, 0150, 0227, 0302, 
  0141, 0354, 0216, 0272, 0112, 0003, 0306, 0207, 0204, 0320, 0100, 0210, 0353, 0344, 0266, 0036, 
  0104, 0244, 0111, 0227, 0244, 0154, 0373, 0367, 0244, 0353, 0064, 0366, 0221, 0064, 0014, 0132, 
  0244, 0365, 0324, 0071, 0216, 0375, 0372, 0251, 0323, 0172, 0041, 0345, 0032, 0345, 0004, 0022, 
  0214, 0116, 0210, 0271, 0302, 0014, 0171, 0101, 0150, 0332, 0357, 0224, 0067, 0061, 0310, 0116, 
  0145, 0137, 0256, 0251, 0042, 0136, 0057, 0117, 0050, 0303, 0156, 0371, 0153, 0303, 0141, 0351, 
  0004, 0132, 0113, 0032, 0027, 0032, 0011, 0207, 0014, 0373, 0001, 0203, 0030, 0131, 0100, 0264, 
  0004, 0256, 0030, 0150, 0210, 0231, 0061, 0056, 0120, 0005, 0321, 0370, 0306, 0304, 0010, 0173, 
  0353, 0015, 0073, 0201, 0024, 0046, 0232, 0012, 0276, 0155, 0335, 0324, 0261, 0277, 0122, 0043, 
  0300, 0352, 0133, 0136, 0373, 0302, 0234, 0256, 0211, 0060, 0260, 0346, 0272, 0037, 0100, 0232, 
  0112, 0124, 0212, 0304, 0102, 0174, 0324, 0271, 0147, 0106, 0246, 0126, 0375, 0340, 0145, 0225, 
  0103, 0110, 0165, 0101, 0062, 0240, 0234, 0124, 0034, 0065, 0146, 0244, 0113, 0036, 0161, 0106, 
  0266, 0042, 0106, 0143, 0143, 0162, 0202, 0131, 0027, 0132, 0156, 0217, 0234, 0331, 0017, 0345, 
  0140, 0147, 0021, 0215, 0137, 0257, 0100, 0246, 0144, 0260, 0322, 0167, 0151, 0364, 0221, 0323, 
  0133, 0056, 0062, 0054, 0355, 0147, 0136, 0225, 0126, 0051, 0260, 0174, 0262, 0101, 0064, 0243, 
  0374, 0174, 0331, 0110, 0034, 0147, 0335, 0317, 0304, 0004, 0364, 0127, 0335, 0163, 0227, 0375, 
  0117, 0104, 0206, 0163, 0163, 0144, 0070, 0060, 0062, 0055, 0120, 0056, 0310, 0051, 0220, 0134, 
  0212, 0067, 0011, 0131, 0223, 0064, 0160, 0225, 0344, 0030, 0200, 0074, 0134, 0337, 0337, 0154, 
  0165, 0110, 0203, 0034, 0130, 0112, 0047, 0307, 0301, 0140, 0060, 0152, 0217, 0001, 0344, 0307, 
  0300, 0340, 0166, 0064, 0154, 0013, 0301, 0133, 0216, 0307, 0100, 0340, 0271, 0210, 0045, 0115, 
  0240, 0055, 0012, 0262, 0012, 0377, 0153, 0022, 0141, 0317, 0371, 0351, 0254, 0001, 0364, 0367, 
  0057, 0152, 0064, 0036, 0111, 0221, 0243, 0324, 0024, 0225, 0137, 0274, 0027, 0105, 0376, 0263, 
  0140, 0116, 0012, 0355, 0226, 0172, 0215, 0014, 0065, 0066, 0120, 0146, 0352, 0017, 0144, 0053, 
  0321, 0074, 0144, 0333, 0344, 0124, 0063, 0117, 0265, 0313, 0343, 0251, 0240, 0272, 0001, 0032, 
  0123, 0137, 0230, 0037, 0262, 0260, 0234, 0001, 0373, 0100, 0171, 0310, 0044, 0073, 0344, 0132, 
  0056, 0334, 0243, 0254, 0243, 0017, 0017, 0200, 0153, 0113, 0132, 0316, 0215, 0365, 0211, 0275, 
  0144, 0261, 0334, 0136, 0276, 0132, 0334, 0332, 0155, 0124, 0333, 0251, 0247, 0072, 0070, 0315, 
  0224, 0344, 0071, 0073, 0273, 0125, 0265, 0322, 0024, 0167, 0310, 0362, 0377, 0356, 0211, 0101, 
  0054, 0012, 0375, 0173, 0170, 0357, 0106, 0162, 0027, 0152, 0143, 0170, 0320, 0205, 0275, 0352, 
  0336, 0270, 0175, 0377, 0263, 0374, 0002, 0340, 0020, 0310, 0334, 0000, 0050, 0165, 0165, 0141, 
  0171, 0051, 0144, 0145, 0163, 0153, 0164, 0157, 0160, 0057, 0000, 0000, 0004, 0000, 0000, 0000, 
  0102, 0141, 0154, 0163, 0141, 0101, 0142, 0057, 0002, 0000, 0000, 0000
} };
#else /* _MSC_VER */
static const SECTION union { const guint8 data[717]; const double alignment; void * const ptr;}  balsa_ab_resource_data = {
  "\107\126\141\162\151\141\156\164\000\000\000\000\000\000\000\000"
  "\030\000\000\000\254\000\000\000\000\000\000\050\005\000\000\000"
  "\000\000\000\000\002\000\000\000\002\000\000\000\005\000\000\000"
  "\005\000\000\000\113\120\220\013\001\000\000\000\254\000\000\000"
  "\004\000\114\000\260\000\000\000\264\000\000\000\324\265\002\000"
  "\377\377\377\377\264\000\000\000\001\000\114\000\270\000\000\000"
  "\274\000\000\000\012\067\312\360\004\000\000\000\274\000\000\000"
  "\012\000\166\000\310\000\000\000\262\002\000\000\124\214\331\133"
  "\000\000\000\000\262\002\000\000\010\000\114\000\274\002\000\000"
  "\300\002\000\000\151\377\210\203\003\000\000\000\300\002\000\000"
  "\010\000\114\000\310\002\000\000\314\002\000\000\157\162\147\057"
  "\003\000\000\000\057\000\000\000\000\000\000\000\141\142\055\155"
  "\141\151\156\056\165\151\000\000\146\016\000\000\001\000\000\000"
  "\170\332\325\227\115\117\303\060\014\206\357\374\212\150\227\302"
  "\141\354\216\272\112\003\306\207\204\320\100\210\353\344\266\036"
  "\104\244\111\227\244\154\373\367\244\353\064\366\221\064\014\132"
  "\244\365\324\071\216\375\372\251\323\172\041\345\032\345\004\022"
  "\214\116\210\271\302\014\171\101\150\332\357\224\067\061\310\116"
  "\145\137\256\251\042\136\057\117\050\303\156\371\153\303\141\351"
  "\004\132\113\032\027\032\011\207\014\373\001\203\030\131\100\264"
  "\004\256\030\150\210\231\061\056\120\005\321\370\306\304\010\173"
  "\353\015\073\201\024\046\232\012\276\155\335\324\261\277\122\043"
  "\300\352\133\136\373\302\234\256\211\060\260\346\272\037\100\232"
  "\112\124\212\304\102\174\324\271\147\106\246\126\375\340\145\225"
  "\103\110\165\101\062\240\234\124\034\065\146\244\113\036\161\106"
  "\266\042\106\143\143\162\202\131\027\132\156\217\234\331\017\345"
  "\140\147\021\215\137\257\100\246\144\260\322\167\151\364\221\323"
  "\133\056\062\054\355\147\136\225\126\051\260\174\262\101\064\243"
  "\374\174\331\110\034\147\335\317\304\004\364\127\335\163\227\375"
  "\117\104\206\163\163\144\070\060\062\055\120\056\310\051\220\134"
  "\212\067\011\131\223\064\160\225\344\030\200\074\134\337\337\154"
  "\165\110\203\034\130\112\047\307\301\140\060\152\217\001\344\307"
  "\300\340\166\064\154\013\301\133\216\307\100\340\271\210\045\115"
  "\240\055\012\262\012\377\153\022\141\317\371\351\254\001\364\367"
  "\057\152\064\036\111\221\243\324\024\225\137\274\027\105\376\263"
  "\140\116\012\355\226\172\215\014\065\066\120\146\352\017\144\053"
  "\321\074\144\333\344\124\063\117\265\313\343\251\240\272\001\032"
  "\123\137\230\037\262\260\234\001\373\100\171\310\044\073\344\132"
  "\056\334\243\254\243\017\017\200\153\113\132\316\215\365\211\275"
  "\144\261\334\136\276\132\334\332\155\124\333\251\247\072\070\315"
  "\224\344\071\073\273\125\265\322\024\167\310\362\377\356\211\101"
  "\054\012\375\173\170\357\106\162\027\152\143\170\320\205\275\352"
  "\336\270\175\377\263\374\002\340\020\310\334\000\050\165\165\141"
  "\171\051\144\145\163\153\164\157\160\057\000\000\004\000\000\000"
  "\102\141\154\163\141\101\142\057\002\000\000\000" };
#endif /* !_MSC_VER */

static GStaticResource static_resource = { balsa_ab_resource_data.data, sizeof (balsa_ab_resource_data.data) - 1 /* nul terminator */, NULL, NULL, NULL };
extern GResource *balsa_ab_get_resource (void);
GResource *balsa_ab_get_resource (void)
{
  return g_static_resource_get_resource (&static_resource);
}
/*
  If G_HAS_CONSTRUCTORS is true then the compiler support *both* constructors and
  destructors, in a sane way, including e.g. on library unload. If not you're on
  your own.

  Some compilers need #pragma to handle this, which does not work with macros,
  so the way you need to use this is (for constructors):

  #ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
  #pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(my_constructor)
  #endif
  G_DEFINE_CONSTRUCTOR(my_constructor)
  static void my_constructor(void) {
   ...
  }

*/

#ifndef __GTK_DOC_IGNORE__

#if  __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) static void __attribute__((constructor)) _func (void);
#define G_DEFINE_DESTRUCTOR(_func) static void __attribute__((destructor)) _func (void);

#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
/* Visual studio 2008 and later has _Pragma */

#include <stdlib.h>

#define G_HAS_CONSTRUCTORS 1

/* We do some weird things to avoid the constructors being optimized
 * away on VS2015 if WholeProgramOptimization is enabled. First we
 * make a reference to the array from the wrapper to make sure its
 * references. Then we use a pragma to make sure the wrapper function
 * symbol is always included at the link stage. Also, the symbols
 * need to be extern (but not dllexport), even though they are not
 * really used from another object file.
 */

/* We need to account for differences between the mangling of symbols
 * for Win32 (x86) and x64 programs, as symbols on Win32 are prefixed
 * with an underscore but symbols on x64 are not.
 */
#ifdef _WIN64
#define G_MSVC_SYMBOL_PREFIX ""
#else
#define G_MSVC_SYMBOL_PREFIX "_"
#endif

#define G_DEFINE_CONSTRUCTOR(_func) G_MSVC_CTOR (_func, G_MSVC_SYMBOL_PREFIX)
#define G_DEFINE_DESTRUCTOR(_func) G_MSVC_DTOR (_func, G_MSVC_SYMBOL_PREFIX)

#define G_MSVC_CTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _wrapper(void) { _func(); g_slist_find (NULL,  _array ## _func); return 0; } \
  __pragma(comment(linker,"/include:" _sym_prefix # _func "_wrapper")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _wrapper;

#define G_MSVC_DTOR(_func,_sym_prefix) \
  static void _func(void); \
  extern int (* _array ## _func)(void);              \
  int _func ## _constructor(void) { atexit (_func); g_slist_find (NULL,  _array ## _func); return 0; } \
   __pragma(comment(linker,"/include:" _sym_prefix # _func "_constructor")) \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) int (* _array ## _func)(void) = _func ## _constructor;

#elif defined (_MSC_VER)

#define G_HAS_CONSTRUCTORS 1

/* Pre Visual studio 2008 must use #pragma section */
#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (*p)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined(__SUNPRO_C)

/* This is not tested, but i believe it should work, based on:
 * http://opensource.apple.com/source/OpenSSL098/OpenSSL098-35/src/fips/fips_premain.c
 */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  init(_func)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void);

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  fini(_func)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void);

#else

/* constructors not supported for this compiler */

#endif

#endif /* __GTK_DOC_IGNORE__ */

#ifdef G_HAS_CONSTRUCTORS

#ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(resource_constructor)
#endif
G_DEFINE_CONSTRUCTOR(resource_constructor)
#ifdef G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(resource_destructor)
#endif
G_DEFINE_DESTRUCTOR(resource_destructor)

#else
#warning "Constructor not supported on this compiler, linking in resources will not work"
#endif

static void resource_constructor (void)
{
  g_static_resource_init (&static_resource);
}

static void resource_destructor (void)
{
  g_static_resource_fini (&static_resource);
}
