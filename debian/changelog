balsa (2.5.9-0.1) experimental; urgency=medium

  * Non-maintainer upload
  * Avoid storing generated files in git packaging repo
  * New upstream release (Closes: #911558)
  * Fix up buildd-attribution in historic changelog entry
  * Rules-Requires-Root: no
  * Move to dh 12
  * Drop dbgsym migration, completed in debian buster
  * Standards-Version: bump to 4.5.0 (no changes needed)
  * Consolidate cleanup into debian/clean
  * Drop unneeded lintian override
  * Minor cleanup patches (submitted upstream)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 13 Feb 2020 13:44:26 -0500

balsa (2.5.6-2) unstable; urgency=medium

  * Restore -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 23 Dec 2018 17:17:25 -0500

balsa (2.5.6-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 11:31:01 -0400

balsa (2.5.5-2) unstable; urgency=medium

  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 10:56:07 -0400

balsa (2.5.5-1) unstable; urgency=medium

  * New upstream release
  * Build-Depend on libgnutls28-dev
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Drop build-ssl-1.1.patch: Applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 18 Mar 2018 23:43:56 -0400

balsa (2.5.3-4) unstable; urgency=medium

  * Bump debhelper compat to 11
  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 18 Dec 2017 11:10:17 -0500

balsa (2.5.3-3) unstable; urgency=medium

  * Fix FTBFS with openssl 1.1.0, thanks to Fedora for the patch (Closes:
    #828245)
  * Bump Standards-Version to 4.1.2 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Sun, 10 Dec 2017 18:09:25 +0100

balsa (2.5.3-2) unstable; urgency=medium

  * Upload to unstable
  * debian/control.in: Bump Standards-Version to 4.0.0 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Fri, 30 Jun 2017 13:55:19 +0200

balsa (2.5.3-1) experimental; urgency=medium

  [ Emilio Pozuelo Monfort ]
  * Switch libgpgme11-dev build-dependency to libgpgme-dev.

  [ Laurent Bigonville ]
  * New Major upstream release:
    - Use GTK+3 instead of GTK+2 and old GNOME2 libs
    - Use libsecret instead of gnome-keyring directly
    - Use GSpell instead of GTKSpell
    - Use Webkit2GTK instead of WebkitGTK
    - No more deprecated GNOME macro's in the configure (Closes: #829850)
  * debian/control.in:
    - Drop cdbs build-dependency, we are using dh7 sequences now
    - Bump debhelper build-dependency
    - Bump Standards-Version to 3.9.8 (no further changes)
    - Adjusts build-dependencies
    - Bump ca-certificates to recommends
    - Add python3-html2text to the recommends and build-dependencies, this is
      needed to quote HTML only mails
    - Drop gnome-icon-theme, not needed anymore with GTK3 applications
    - Add pinentry-gnome3 | pinentry-x11 to the dependencies (Closes: #464818)
    - Build-depends against libssl1.0-dev instead of libssl-dev
    - Drop balsa-dbg package and rely on the automatically build one.
  * debian/rules:
    - Switch to dh7 sequences
    - Drop deprecated configure flags
    - Build with the S/MIME support
    - Build with GSpell instead of GTKSpell
    - Build with webkit2 instead of webkit
    - Do not override GTK_UPDATE_ICON_CACHE anymore, upstream build system is
      not calling it anymore
    - Pass --as-needed to dh_autoreconf
    - Enable all hardening flags
  * debian/compat: Bump debhelper compatibility to 9
  * Drop debian/patches/99_ltmain_as-needed.patch and pass --as-needed to
    dh_autoreconf instead

 -- Laurent Bigonville <bigon@debian.org>  Thu, 05 Jan 2017 17:00:15 +0100

balsa (2.4.12-3) unstable; urgency=medium

  * Team upload.

  [ Michael Biebl ]
  * Remove Debian menu entry.

  [ Alberto Garcia ]
  * debian/control.in:
    - Replace build dependency on libwebkit-dev with libwebkitgtk-dev
      (Closes: #818294).
  * debian/balsa.lintian-overrides:
    - Override 'possible-gpl-code-linked-with-openssl'. Balsa does allow
      linking the code against OpenSSL.

 -- Alberto Garcia <berto@igalia.com>  Wed, 16 Mar 2016 15:07:46 +0200

balsa (2.4.12-2) unstable; urgency=medium

  * Team upload.
  * Use webkit instead of gtkhtml.

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 30 Aug 2015 19:57:28 +0200

balsa (2.4.12-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/gmime-2.6.patch: Removed, merged upstream.
  * Bump Standards-Version to 3.9.3.

 -- Michael Biebl <biebl@debian.org>  Sun, 13 May 2012 21:52:35 +0200

balsa (2.4.11-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/99_ltmain_as-needed.patch: Refreshed.
  * Drop usage of dh_buildinfo.
  * Build against gmime 2.6. Patch cherry-picked from upstream Git.
  * Use dh-autoreconf to update the build system.

 -- Michael Biebl <biebl@debian.org>  Wed, 14 Mar 2012 15:12:04 +0100

balsa (2.4.10-2) unstable; urgency=low

  * debian/rules:
    - Pass --without-nm when building on non-linux arch

 -- Laurent Bigonville <bigon@debian.org>  Sat, 23 Jul 2011 20:41:01 +0200

balsa (2.4.10-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream stable releases (Closes: #577565).
    - IMAP compression (COMPRESS=DEFLATE) when supported by server
    - making opening attachment easier by saving them in a directory
    - downloadable images when using Webkit html widget
    - faster opening of the compose window.
    - text searching enabled in the WebKit html viewer.
    - options to hide toolbars and other parts of the UI (useful
      on netbooks etc).
    - For laptop/wifi users: not trying to reach POP3 servers while
      disconnected, and instead checking immediately on reconnection.
    - infinite loop avoided (Closes: #568298).
    - avoid crashes when manually verifing GPG signature.
  * debian/control*
    - bump standards version to 3.8.4, no extra change is needed
    - bump libgtk2.0-dev build-dep to 2.18.0
    - added libnm-glib-dev (Linux) and gnome-icon-theme to build-deps

  [ Emilio Pozuelo Monfort ]
  * debian/watch:
    - Fixed, wasn't tracking anything.
  * debian/control.in:
    - balsa-dbg is section debug.
    - Let balsa-dbg depend on ${misc:Depends}.

  [ Laurent Bigonville ]
  * New upstream stable releases
    - Add compatibility for libnotify >= 0.7 (Closes: #630263)
  * debian/control.in:
    - Add gnome-doc-utils and libcanberra-gtk-dev as build-dependencies
    - Build-depends against libunique-dev instead of libbonobo2-dev
    - Fix descriptions to please lintian
    - Use architecture wildcards instead of negated architectures
    - Bump Standards-Version to 3.9.2 (no further changes)
  * debian/rules:
    - Enable libcanberra support
    - Enable libunique support
    - Pass --as-needed to LDFLAGS
    - Drop simple-patchsys.mk include
    - Drop --with-gtkprint configure flag, not supported anymore
  * Switch to dpkg-source 3.0 (quilt) format
  * debian/copyright: Add missing copyright statements
  * debian/patches/99_ltmain_as-needed.patch: Fix --as-needed logic

 -- Laurent Bigonville <bigon@debian.org>  Sat, 23 Jul 2011 19:09:34 +0200

balsa (2.4.1-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream stable releases (Closes: #539507).
    - Continuing cleanup work: removing obsolete library calls.
    - Partial workaround for the deadlock in xcb library code.
    - Experimental use of libcanberra for sound.
    - Many small bugfixes.
  * debian/control*:
    - update homepage address (Closes: #537058).
    - bump libglib2.0-dev build-dep to 2.16.0
    - bump libgtk2.0-dev build-dep to 2.14.0
    - bump Gmime build-dep to libgmime-2.4 (Closes: #549053).
    - bump standards version to 3.8.3, no extra change is needed
    - drop libgnomevfs2-dev build-dep, not needed anymore
    - replace scrollkeeper build-dep with rarian-compat
  * Dropped 01_desktop_categories.patch, merged upstream

  [ Josselin Mouette ]
  * Update watch file and download URL.

 -- Alan Baghumian <alan@technotux.org>  Thu, 22 Oct 2009 20:15:56 -0700

balsa (2.3.28-1) unstable; urgency=low

  * New upstream stable releases (Closes: #527005).
    - Fix the IMAP message threading regression.
    - Use GIO library when available.
    - Improve GMail IMAP compatibility.
    - Fixed GNOME bugs 509434, 567323, 566426, 566420, 566415
  * debian/control/*
    - bump standards version to 3.8.1, no extra change is needed
    - added intltool to build-deps
    - replace libltdl3-dev build-dep with libltdl-dev

 -- Alan Baghumian <alan@technotux.org>  Wed, 20 May 2009 18:32:23 -0700

balsa (2.3.25-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream stable releases (Closes: #483546).
    - GtkSourceView2 support
    - Improved scanning of mailbox trees
    - Support for accessing files over gnome-vfs
    - Rudimentary support for sending text/html messages
  * Updated debian/control*:
    - bump libgmime-2.0-2-dev build-dep to 2.2.21, earlier versions
      have known bugs.
    - use libgtksourceview2.0-dev, its now officially supported
  * Updated debian/rules:
    - use libgtksourceview2.0-dev, its now officially supported

  [ Josselin Mouette ]
  * New upstream release.

 -- Josselin Mouette <joss@debian.org>  Sun, 15 Jun 2008 13:13:29 +0200

balsa (2.3.21-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream stable release:
    - Migration from GnomeUI to GtkUIManager
    - Rubrica address book support
    - Bugs fixed: GNOME: 481842, 488432, 496316; Redhat: 414361
  * Merged with previous changelog entry
  * Updated debian/control:
    - Bump libgmime-2.0 dependency to 2.2.11 (Closes: #443451).
    - Added libxml2-dev to build deps (needed by --with-rubrica)
  * Updated debian/rules:
    - Added --with-libnotify to enable event notifications
    - Added --with-rubrica to support Rubrica address book

  [ Sven Arvidsson ]
  * Add a -dbg package.
    - Bump up build-dep of cdbs to >= 0.4.37.

  [ Josselin Mouette ]
  * Patch from Ubuntu to properly remove .gmo and .desktop files in
    the clean target. Closes: #449125, thanks Marco Rodrigues.
  * Update menu file to the new layout.
  * 01_desktop_categories.patch: fix categories of
    balsa.desktop to conform to the menu specification.
  * Standards version is 3.7.3.
  * Fix encoding of the copyright file.
  * Use the Homepage field.

 -- Josselin Mouette <joss@debian.org>  Sat, 12 Jan 2008 18:15:45 +0100

balsa (2.3.20-1) unstable; urgency=high

  * New upstream release:
    - Fixed a buffer overflow in the IMAP code

 -- Alan Baghumian <alan@technotux.org>  Thu, 13 Sep 2007 18:11:50 +0330

balsa (2.3.19-1) unstable; urgency=low

  [ Kilian Krause ]
  * Added get-orig-source target.

  [ Loic Minier ]
  * Set Maintainer to Debian GNOME Maintainers.

  [ Alan Baghumian ]
  * New upstream stable release:
    - Reflow improvements.
    - Build problems against steaming-hot GNOME fixed.
    - Made BINARY and ESEARCH IMAP extensions usage more robust.
  * Merged with the previous changelog entry.

  [ Loic Minier ]
  * Bump up libgmime-2.0-2-dev build-dep to >= 2.2.10 and let balsa conflict
    with libgmime-2.0-2 << 2.2.10 to ensure we build against the new internal
    GObject ABI: it seems the internal size of the GObject changed, and this
    breaks the GObject subclassing of g_mime_stream_fs done in balsa to add
    locking support, GLib detects the size difference and aborts; it's not
    clear whether the GObject interface is part of the upstream ABI or not;
    see #434499 and friends.

 -- Loic Minier <lool@dooz.org>  Sun, 02 Sep 2007 18:58:48 +0200

balsa (2.3.17-1) unstable; urgency=low

  * New upstream release
     - Checks validity of server APOP challenge
     - Adds ability to store VCARD signatures
     - Uses ESEARCH IMAP extension if available
     - Message wrapping fixes
     - Few bug fixes

 -- Alan Baghumian <alan@technotux.org>  Thu, 05 Jul 2007 09:15:13 +0330

balsa (2.3.16-1) unstable; urgency=low

  * New upstream release, fixing bugs
     - fix message threading regression
     - reorganize "Identity" dialog

 -- Alan Baghumian <alan@technotux.org>  Wed, 30 May 2007 14:06:53 +0330

balsa (2.3.15-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream release (Closes: #419723).
     - Use GtkPrint infrastructure if available
     - Handle LDAP referrals
     - Nicer citation marks
     - Initial libnotify support
     - IMAP BINARY support
  * Dropped relibtoolizing patch, not needed
    anymore
  * Updated debian/rules
     - Minor clean-ups
     - Added --enable-threads and --with-gtkprint to the
       build options
  * Updated debian/control
     - Added libnotify-dev and gnome-pkg-tools to build-deps
     - Wrapped build-deps and deps
     - Bump libgtk2.0-dev to 2.10.0
     - Bump libgmime-2.0-2-dev to 2.2.5
     - Changed libgtkhtml3.8-dev build-dep to libgtkhtml3.14-dev
     - Removed libgnomeprint2.2-dev and lignomeprintui2.2-dev
       from build-deps, GtkPrint is enabled
  * debian/README.Debian
     - Minor clean-ups

  [ Loic Minier ]
  * Cleanups; install pixmap and symlinks via debian/balsa.install and .links.

 -- Alan Baghumian <alan@technotux.org>  Tue, 15 May 2007 14:11:04 +0330

balsa (2.3.13-2) unstable; urgency=low

  * Rebuild against libgmime-2.0-2-dev which supersedes libgmime2.1-dev.
    (Closes: #383157)

 -- Loic Minier <lool@dooz.org>  Thu, 17 Aug 2006 18:41:48 +0200

balsa (2.3.13-1) unstable; urgency=low

  * New upstream release.
    - Fixed setting message size correctly. (closes: #351385)
    - Has working help button. (closes: #320858)
  * GOption patch obsolete. [debian/patches/00_popt_goption.patch]
  * Update list of authors. [debian/copyright]
  * Compile with gtksourceview. [debian/rules]
  * Install extra mime icons. [debian/rules]
  * Bump Standards-Version to 3.7.2. No changes needed.
    [debian/control.in]
  * Add Provides: imap-client. [debian/control.in]
  * Remove Suggests: gnome-pim. [debian/control.in]
  * Add GNOME build-deps. [debian/control.in]
  * Update relibtoolize patch using autoconf 2.59.
    [debian/patches/50_relibtoolize.patch]

 -- Oystein Gisnas <oystein@gisnas.net>  Mon, 31 Jul 2006 11:36:55 +0200

balsa (2.3.8-3) unstable; urgency=low

  * debian/patches/00_popt_goption.patch:
  - new patch from upstream CVS to replace the deprecated popt with GOption.
    Thanks to Matt Kraai for isolating the changes. Closes: #360379.

 -- Guilherme de S. Pastore <gpastore@debian.org>  Wed, 26 Apr 2006 00:10:23 -0300

balsa (2.3.8-2) unstable; urgency=low

  * Rebuild against GtkHTML 3.8.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Tue, 10 Jan 2006 19:47:41 +1100

balsa (2.3.8-1) unstable; urgency=low

  [ Andrew Lau ]
  * New upstream release.
    - GtkSpell support.
    - X-Face support.
    - Patches merged: 00_piping-fix.patch
  * [debian/rules]:
    - Revert usage of ld's --as-needed option.
  * Debian Standards-Version: 3.6.2.2
  * Debhelper compatibility: 5

  [ Loic Minier ]
  * Relibtoolize.

 -- Loic Minier <lool@dooz.org>  Sun,  8 Jan 2006 17:10:12 +0100

balsa (2.3.4-3+b1) unstable; urgency=low

  * Binary-only non-maintainer upload for i386; no source changes.
  * Rebuild against libssl0.9.8 (>= 0.9.8a-1) to get the symbol-versioned lib

 -- Debian/i386 Build Daemon <buildd_i386-cyberhq@buildd.debian.org>  Mon, 28 Nov 2005 02:39:55 -0800

balsa (2.3.4-3) unstable; urgency=low

  * Rebuild against openssl >= 0.9.8-1 for the new soname (libssl0.9.8).
    [debian/control.in, debian/control]
  * Revert the build-dep version bump of 2.3.4-2 and build-depend on the older
    pspell (>= 0.50).
    [debian/control.in, debian/control]
  * Fix formatting of changelog.

 -- Loic Minier <lool@dooz.org>  Sat,  8 Oct 2005 15:10:24 +0200

balsa (2.3.4-2) unstable; urgency=medium

  * CXX transition:
    - Rebuild against libpspell-dev (>= 0.60.3-3) to support libaspell15c2.
  * Add dependency on gnome-icon-theme.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sun, 17 Jul 2005 01:02:35 +1000

balsa (2.3.4-1) unstable; urgency=medium

  * New upstream release.
    - Possible buffer overflow in IMAP code fixed.
    - Default to TrueType fonts for printing.
      (closes: #202579)
    - FHS compliance: /var/mail
      (closes: #310488)
    - Patches merged: 00_openpgp-decrypt.patch, 01_date-utf8.patch,
                      02_address-book-ldap.patch
  * Debian Standards-Version to 3.6.2
  * Rebuild against GtkHTML 3.6.
  * Fix "pipe through" feature.
    + [debian/patches/00_piping-fix.patch]

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sat,  9 Jul 2005 00:21:23 +1000

balsa (2.3.3-1) unstable; urgency=low

  * New upstream release.
    - SQLite 3.x support.
    - Safely handle unreadable /etc/mailname file.
      (closes: #312083)
    - Patches merged: 00_extern_addrbook.patch, 01_imap-folders.patch
  * Fix decryption of mutlipart and inline OpenPGP messages (RFC 2440/3156).
    + [debian/patches/00_openpgp-decrypt.patch]
  * Convert the formatted date from the locale charset to UTF-8
    + [debian/patches/01_date-utf8.patch]
  * Add missing include for i18n.h to address-book-ldap.c
    + [debian/patches/02_address-book-ldap.patch]
  * [debian/rules]:
    - Record compile-time dependency versions using dh_buildinfo

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Thu,  9 Jun 2005 03:57:40 +1000

balsa (2.3.2-2) unstable; urgency=medium

  * Fix IMAP folder scanning segfault on 64-bit platforms.
    + [debian/patches/01_imap-folders.patch]

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sat,  4 Jun 2005 00:27:02 +1000

balsa (2.3.2-1) unstable; urgency=low

  * New upstream release.
    - Improved manpage.
      (closes: #269787)
      Thanks: Siward de Groot <siward@wanadoo.nl>
    - Little Brother Database support fixed.
      (closes: #306556)
    - New preference manager and configuration file format.
      See README.Debian for upgrade notes.
    - Patch merged: 00_druid_init.patch
  * Properly auto-expand multi-address contacts in external addressbooks.
    + [debian/patches/00_extern_addrbook.patch]
      CVS: libbalsa/address-book-extern.c (r1.17)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Fri, 13 May 2005 23:45:36 +1000

balsa (2.3.0-2sarge1) testing; urgency=medium

  * Backport fixes from Balsa 2.3.1 and 2.3.2 for Debian "Sarge"
    + [debian/patches/01_extern_addrbook.patch]:
      - Re-enables external address books in the UI.
        (closes: #306553)
      - Fixes segfaults caused by accessing them.
        (closes: #306556)
    + [debian/patches/02_improved_manpage.patch]
      - Backported updated manpage from 2.3.2.
        Thanks: Siward de Groot <siward@wanadoo.nl>
        (closes: #269787)
    + [debian/patches/03_restored_icon.patch]
      - One line fix to restore window manager icon.
        (closes: #298579)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Thu, 12 May 2005 23:41:01 +1000

balsa (2.3.1-1) unstable; urgency=medium

  * New upstream release.
    - Window Manager icon now displaying.
      (closes: #298579)
    - External address book support fixed.
      (closes: #306553)
    - Patch merged: 00_filters.patch
  * Fixed new user initialisation segfault.
    + [debian/patches/00_druid.patch]
      CVS: libinit_balsa/balsa-druid-page-user.c (r1.2)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Tue,  3 May 2005 16:41:49 +1000

balsa (2.3.0-2) unstable; urgency=medium

  * Correct bugs in filters code.
    + [debian/patches/00_filters.patch]
      CVS: libbalsa/mailbox_local.c (r1.132)
  * [debian/README.Debian]:
    - Provide new URL for Albrecht Dreß's "Secure Mail with Balsa" HOWTO
  * [debian/rules]:
    - Deflate dependencies via ld's --as-needed option.
      Thanks: Jeff Waugh <jdub@gnome.org>
    - Disable gtk-update-icon-cache for now.
      (closes: #298181)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Tue,  1 Mar 2005 02:36:14 +1100

balsa (2.3.0-1) unstable; urgency=low

  * New upstream release.
    - New address completion widget.
      (closes: #290642, #293305)
    - Patch merged: 00_configure-smime.patch

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sat, 26 Feb 2005 15:16:33 +1100

balsa (2.2.6-1) unstable; urgency=low

  * New upstream release.
    - Mailbox counters fixed
      (closes: #278617)
    - Windows-125x codesets included in list of fallback encodings.
      (closes: #248827)
  * S/MIME support disabled due to requirement of alpha version (1.9) of GnuPG.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sun, 21 Nov 2004 15:36:42 +1100

balsa (2.2.5-2) unstable; urgency=medium

  * Rebuild against GtkHTML 3.2.
    (closes: #270881)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Thu,  7 Oct 2004 03:01:15 +1000

balsa (2.2.5-1) unstable; urgency=medium

   * New upstream release.
    - GnuPG support fully functional again.
      (closes: #272189)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sun,  3 Oct 2004 20:47:39 +1000

balsa (2.2.4-1) unstable; urgency=low

  * New upstream release.
    - IMAP password quering code cleanup
      (closes: #267949)
    - Patch merged: 00_64-bit.patch
  * GPE Palmtop Environment address book support enabled.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sun, 29 Aug 2004 01:13:23 +1000

balsa (2.2.3-1) unstable; urgency=low

  * New upstream release.
  * Fix build failures on 64-bit platforms.
    Thanks: Andreas Jochens <aj@andaco.de>
    - Patch: 00_64-bit.patch
      (closes: #266298)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Wed, 18 Aug 2004 00:09:26 +1000

balsa (2.2.2-1) unstable; urgency=low

  * New upstream stable branch release.
    (closes: #248556)
    Bugs fixed:
    - eSMTP library interaction
      (closes: #191846)
    - Attachment window
      (closes: #193960)
    - GPG handling
      (closes: #207533)
    - Patches merged: 00_rfc3156_2004-03-31.patch
  * Rebuilt using gcrypt11/gnutls11.
    (closes: #264412, #264746)
  * GtkHTML 3.1 and GMime 2.1 support.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sun, 15 Aug 2004 01:08:49 +1000

balsa (2.0.18-1) testing; urgency=medium

  * New upstream release.
    - Last release of the Balsa 2.0 stable branch
    - Patches merged: 00_rfc3156_2004-03-31.patch
  * Rebuild against libgnutls11.
  * Uploading to testing as 2.0.17-2 was effectively a prerelease of this new
    version.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Sat, 28 Aug 2004 23:57:49 +1000

balsa (2.0.17-2) unstable; urgency=low

  * Balsa doesn't seem to support GtkHTML 3.1 for HTML emails yet -- reverted
    to 3.0.
  * Added libgail-dev to Depends.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Thu, 29 Apr 2004 18:02:25 +1000

balsa (2.0.17-1) unstable; urgency=low

  * New upstream release.
  * Patches applied: 00_rfc3156_2004-03-31.patch
    - GPGME interaction fixes.
      Thanks: Albrecht Dreß <albrecht.dress@arcor.de>
      (closes: #242874, #244584)

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Wed, 21 Apr 2004 23:38:27 +1000

balsa (2.0.16-2) unstable; urgency=medium

  * GNOME team upload.
  * [debian/control.in] Bumped libgtkhtml build dependency and rebuild against
    current libgtkhtml to get an installable package.
    (closes: #244006, #243263)
  * [debian/patches/00_autogen.patch] Updated to cut down on library
    dependencies.
  * [debian/menu] Quote values.

 -- J.H.M. Dassen (Ray) <jdassen@debian.org>  Fri, 16 Apr 2004 19:31:56 +0200

balsa (2.0.16-1) unstable; urgency=low

  * New upstream release.
    - GtkHTML 3.0 support allows for HTML email printing.
      (closes: #180899)
    - Displays UTF-8 hint if charset doesn't match input method.
      (closes: #202578)
    - Delete button in "Message from..." deletes the open message.
      (closes: #213736)
    - IMAP synchronization button in toolbar.
      (closes: #215941)
    - GPGME support upgraded.
      (closes: #221605)
  * Patches applied
    - 00_autogen.patch: work around -pthread linking build failures.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Tue, 20 Jan 2004 20:02:44 +1100

balsa (2.0.15-1) unstable; urgency=low

  * New upstream release.

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Mon, 29 Sep 2003 15:59:00 +1000

balsa (2.0.14-1) unstable; urgency=low

  * New upstream release.
  * Debian Standards-Version: 3.6.1

 -- Andrew Lau <netsnipe@users.sourceforge.net>  Wed, 27 Aug 2003 02:05:22 +1000

balsa (2.0.13-1) unstable; urgency=low

  * New upstream release.
    - Window Manager icon now displaying.
      (closes: #193994)
    - Config not found warning removed.
      (closes: #194673)
    - Saves and restores printing settings between sessions.
      (closes: #200426)
    - README now notes that GnuPG passphrase with national characters must be
      UTF-8 encoded.
      (closes: #201092)
    - Fixed password prompt crash on POP3 startup check.
      (closes: #201124)
    - External editor now selected in GNOME's Control Center.
      (closes: #202461)
    - Timestamps now default to user's locale.
      (closes: #202580)
  * Debian Standards-Version: 3.6.0

 -- Andrew Lau <netsnipe@debianplanet.org>  Tue, 29 Jul 2003 01:04:10 +1000

balsa (2.0.12-1) unstable; urgency=low

  *  New upstream release.
     - Nested Maildir folder support.
       (closes: #175067)
     - Verbose SMTP error diagnostics.
       (closes: #191848)
     - Opening IMAP inbox on startup crash fixed.
       (closes: #191853)
  *  Switch from Debhelper to Common Build System.

 -- Andrew Lau <netsnipe@debianplanet.org>  Mon,  7 Jul 2003 19:57:52 +1000

balsa (2.0.11-1) unstable; urgency=low

  * New upstream release.
    - Fixed broken image links in help.
      (closes: #189046)
    - Copy and pasting of headers enabled.
      (closes: #193537)
  * Standards-Version: 3.5.10
  * Added scrollkeeper to dependencies.
    (closes: #191233)

 -- Andrew Lau <netsnipe@debianplanet.org>  Sun, 18 May 2003 17:35:09 +1000

balsa (2.0.10-1) unstable; urgency=low

  * New upstream release.
    - Wrapping cleanups.
      (closes: #103400)
    - Large set of user interface fixes.
      (closes: #180651)
  * Debian Standards-Version: 3.5.9
  * Enabled experimental GnuPG Made Easy (GPGME) support.

 -- Andrew Lau <netsnipe@debianplanet.org>  Fri, 28 Mar 2003 01:17:53 +1100

balsa (2.0.9-1) unstable; urgency=low

  * New upstream release.
  * Fixes to $(DEB_BUILD_OPTIONS) handling.

 -- Andrew Lau <netsnipe@debianplanet.org>  Tue, 25 Feb 2003 15:57:49 +1100

balsa (2.0.8-1) unstable; urgency=low

  * New upstream release.

 -- Andrew Lau <netsnipe@debianplanet.org>  Mon, 17 Feb 2003 23:08:45 +1100

balsa (2.0.7-1) unstable; urgency=low

  * New upstream release.
    - Handles IMAP usernames containing metacharacters.
      (closes: #179092)

 -- Andrew Lau <netsnipe@debianplanet.org>  Mon, 10 Feb 2003 21:29:26 +1100

balsa (2.0.6-1) unstable; urgency=low

  * New upstream release.
    - Now handles drop-down lists in HTML emails.
      (closes: #175909)
    - Improved Norwegian character support.
      (closes: #176010)

 -- Andrew Lau <netsnipe@debianplanet.org>  Thu, 30 Jan 2003 17:08:32 +1100

balsa (2.0.5-2) unstable; urgency=low

  * New maintainer for Balsa. Many thanks to the previous maintainers:
    - Jules Bean <jules@debian.org>
    - Ole J. Tetlie <olet@debian.org>
    - Stuart Parmenter <pavlov@balsa.net>
    - Martin Schulze <joey@infodrom.org> (Security NMUs).
  * Rebuild with pspell 0.12.2-6 which supports aspell 0.5.
  * Rebuild with GNOME 2.2 printing libraries.
  * Suggests ca-certificates
    (closes: #175406).
  * Fixed incorrect client TLS cert path in README
    (closes: #175407).

 -- Andrew Lau <netsnipe@debianplanet.org>  Sat, 18 Jan 2003 15:00:55 +1100

balsa (2.0.5-1) unstable; urgency=low

  * New upstream version
    - Czech language support added
      (closes: #159737)
    - Signature path now parses ~ as $HOME
      (closes: #172025)
    - Removed invalid help button.
      (closes: #175223)

 -- Andrew Lau <netsnipe@debianplanet.org>  Sun,  5 Jan 2003 17:00:29 +1100

balsa (2.0.4-0.1) unstable; urgency=low

  * Non-maintainer upload.
    (closes: #126058, #156296)
  * OpenSSL exclusion clause now provided by upstream.
  * debian/* rewritten from scratch.
    (closes: #132768, #148629, #172490)
  * Due to aspell .deb version lag (0.33 vs. 0.5), build against
    deprecated pspell instead.

 -- Andrew Lau <netsnipe@debianplanet.org>  Wed, 25 Dec 2002 01:50:34 +1100

balsa (1.2.4-2.2) stable-security; urgency=high

  * Non-maintainer upload by the Security Team
  * Added special detection routine for big/little endianess on MIPS since
    the line "byteorder : {big|little} endian" from /proc/cpuinfo was
    removed as of Linux 2.4.20, resulting in the mips and mipsel buildd
    being unable to build this package.

 -- Martin Schulze <joey@infodrom.org>  Fri,  2 May 2003 07:22:34 +0200

balsa (1.2.4-2.1) stable-security; urgency=high

  * Non-maintainer upload by the Security Team
  * Applied patch from Mutt to fix off-by-one buffer overflow as found by
    Byrial Jensen <byrial@image.dk> (libmutt/imap/util.c, CAN-2003-0167,
    DSA 274)

 -- Martin Schulze <joey@infodrom.org>  Thu,  1 May 2003 17:45:57 +0200

balsa (1.2.4-2) unstable; urgency=low

  * Another library recompile.  Closes:#131683
  * The new libspell-dev should sort out the port compilation probs

 -- Jules Bean <jules@debian.org>  Fri,  1 Feb 2002 21:12:59 +0000

balsa (1.2.4-1) unstable; urgency=low

  * New upstream version (mainly bugfixes)
  * Fixes IMAP password issues (closes:#112067)
  * Remove duplicate Build-Dep (closes:#129218)
  * Provide mail-reader (closes:#129108)
  * Remove Depends: on gnome-pim
  * I plan to keep tracking 1.2.x not 1.3.x for the woody release.

 -- Jules Bean <jules@debian.org>  Tue, 22 Jan 2002 09:11:55 +0000

balsa (1.2.3-2) unstable; urgency=low

  * How I love to do the GNOME libraries dance! closes:#124116

 -- Jules Bean <jules@debian.org>  Fri, 21 Dec 2001 08:18:35 +0000

balsa (1.2.3-1) unstable; urgency=low

  * New upstream version, closes:#118779
  * Replaces early gnome-panel-data, closes:#117042

 -- Jules Bean <jules@pear.presence.net.uk>  Wed, 21 Nov 2001 21:27:08 +0000

balsa (1.2.1-3) unstable; urgency=low

  * Recompile against new libraries, closes:#119168

 -- Jules Bean <jules@debian.org>  Tue, 13 Nov 2001 19:27:47 +0000

balsa (1.2.1-2) unstable; urgency=low

  * Recompile against new libraries, closes:#117333

 -- Jules Bean <jules@debian.org>  Tue, 30 Oct 2001 08:43:40 +0000

balsa (1.2.1-1) unstable; urgency=low

  * New upstream version (yay!).  Highlights include new icons, more D&D
    support, forwarding as attachments, and format=flowed wrapping.

 -- Jules Bean <jules@debian.org>  Mon, 22 Oct 2001 19:33:51 +0100

balsa (1.2.0-2) unstable; urgency=low

  * Fix compilation on certain new(ish) archs by updating config.guess and
    config.sub, thanks to James Troup (Closes:#115025)
  * Recompile against new libraries (*sigh* Closes:#115134)

 -- Jules Bean <jules@debian.org>  Wed, 17 Oct 2001 17:58:51 +0100

balsa (1.2.0-1) unstable; urgency=low

  * New upstream version.

 -- Jules Bean <jules@debian.org>  Thu, 20 Sep 2001 09:04:51 +0100

balsa (1.1.7+1.2pre3-2) unstable; urgency=low

  * Recompile against new libraries. Closes:#111482

 -- Jules Bean <jules@debian.org>  Thu,  6 Sep 2001 21:41:21 +0100

balsa (1.1.7+1.2pre3-1) unstable; urgency=low

  * New upstream version 1.2pre3.

 -- Jules Bean <jules@debian.org>  Fri, 31 Aug 2001 14:31:44 +0100

balsa (1.1.7-4) unstable; urgency=low

  * Remove errant CFLAGS override which probably stops build working,
    which hopefully closes the bug #107016. Required minor patch to make
    main.c compile, many thanks to christophe barbe.
  * Compile with --enable-ldap (closes:#107131). Note to self: this
    introduced a bogus build-depends on libsasl-dev (please read bug
    #96957 to partly understand why)

 -- Jules Bean <jules@debian.org>  Thu, 30 Aug 2001 20:18:19 +0100

balsa (1.1.7-3) unstable; urgency=low

  * Rebuild against up-to-date libraries

 -- Jules Bean <jules@debian.org>  Thu, 23 Aug 2001 10:30:47 +0100

balsa (1.1.7-2) unstable; urgency=low

  * Build-depend on libltdl3-dev and flex (closes:#106902)

 -- Jules Bean <jules@debian.org>  Sat, 28 Jul 2001 18:19:14 +0100

balsa (1.1.7-1) unstable; urgency=low

  * New upstream version (Closes:#79440,#78051,#83975,#83982,#58726, #95726)
  * Highlights include threaded message view and better IMAP support.
  * no more depends on libtdl (closes: #98110)
  * added build-depends (closes: #102770, #70149)
  * no longer kills dpkg (closes: #93917)
  * Josselin Mouette reported that #101052 is closed (closes: #101052)
  * Added dep to gnome-pim (Closes:#74149)
  * Close some very old (and some less old) bug reports which are
      (I truly hope) no longer present:
      (Closes:#55509,#60808,#62839,#69835,#71893,#69780,#75989)
  * Add menu hints (Closes:#80008)
  * added --enable-gtkhtml (closes: #57525)
  * cut'n'paste possible (closes: #58726, #60322, #71873)
  * mailbox list is updated (closes: #101137)
  * added suggests: aspell | ispell (closes: #97858)
  * console errors messages should be disappeared (closes: #83975)
  * typo fixed (closes: #83982)

 -- Jules Bean <jules@debian.org>  Thu, 26 Jul 2001 21:55:14 +0100

balsa (0.9.5-1.0.pre5-1) unstable; urgency=low

  * New upstream version
  * Many upstream improvements, see changelog.

 -- Jules Bean <jules@debian.org>  Thu,  9 Nov 2000 21:34:44 +0000

balsa (0.8.1-2) unstable; urgency=low

  * Recompile removing broken dependency on the helix version of
    gdk-imlib1 (sorry!)
  * Still some problems with the POP code; POP users may be best advised
    to use fetchmail and then use balsa as if with local mail.

 -- Jules Bean <jules@debian.org>  Fri, 25 Aug 2000 10:53:42 +0100

balsa (0.8.1-1) unstable; urgency=low

  * New upstream (long overdue!)

 -- Jules Bean <jules@debian.org>  Sun, 13 Aug 2000 19:39:51 +0100

balsa (0.6.0-1) unstable; urgency=low

  * New upstream
  * New maintainer
  * Many upstream changes, check the changelog.
  * Built with --enable-threads, please report any bugs!

 -- Jules Bean <jules@debian.org>  Wed, 22 Dec 1999 18:52:38 +0000

Balsa (0.5.0-1) frozen unstable; urgency=high

  * New upstream.
  * Many problems fixed.

 -- Stuart Parmenter <pavlov@balsa.net>  Sun, 26 Sep 1999 14:17:31 -0700

balsa (0.4.9.5-1) frozen unstable; urgency=high

  * New upstream.
  * Many problems fixed.

 -- Stuart Parmenter <pavlov@pavlov.net>  Tue, 20 Apr 1999 22:42:13 +0200

balsa (0.4.6.2-1) frozen unstable; urgency=low

  * Cleans up .deps and some config files in the clean target
  * Upstream bugfix only release. Fixes a bug that makes it
    impossible to start balsa without a valid ~/.balsa.

 -- Ole J. Tetlie <olet@debian.org>  Sat, 17 Oct 1998 22:42:13 +0200

balsa (0.4.6-1) unstable; urgency=low

  * New upstream.

 -- Ole J. Tetlie <olet@debian.org>  Thu, 15 Oct 1998 21:18:14 +0200

balsa (0.4.5-1) unstable; urgency=low

  * New upstream release.

 -- Stuart Parmenter <pavlov@innerx.net>  Sun, 23 Aug 1998 14:21:12 -0400

balsa (0.4.0-1) unstable; urgency=low

  * Initial Release.

 -- Stuart Parmenter <pavlov@innerx.net>  Mon, 03 Aug 1998 21:24:58 -0400
